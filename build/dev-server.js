require('./check-versions')(); // 检查 Node 和 npm 版本

var config = require('../config');
if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = JSON.parse(config.dev.env.NODE_ENV)
}

var opn = require('opn')
var path = require('path');
var express = require('express');
var webpack = require('webpack');
var proxyMiddleware = require('http-proxy-middleware');
var webpackConfig = require('./webpack.dev.conf');

// default port where dev server listens for incoming traffic
var port = process.env.PORT || config.dev.port;
// automatically open browser, if not set will be false
var autoOpenBrowser = !!config.dev.autoOpenBrowser;
// Define HTTP proxies to your custom API backend
// https://github.com/chimurai/http-proxy-middleware
var proxyTable = config.dev.proxyTable;

var app = express();
var compiler = webpack(webpackConfig);

var devMiddleware = require('webpack-dev-middleware')(compiler, {
    publicPath: webpackConfig.output.publicPath,
    quiet: true
});

var hotMiddleware = require('webpack-hot-middleware')(compiler, {
    log: false,
    heartbeat: 2000
});

// force page reload when html-webpack-plugin template changes
compiler.plugin('compilation', function (compilation) {
    compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
        hotMiddleware.publish({action: 'reload'});
        cb()
    })
});

// proxy api requests
Object.keys(proxyTable).forEach(function (context) {
    var options = proxyTable[context]
    if (typeof options === 'string') {
        options = {target: options}
    }
    app.use(proxyMiddleware(options.filter || context, options))
});

// -----------------------------------------------------------------------------------------------------------
// var bodyParser = require('body-parser')
// app.use(bodyParser.json())

// WeChatConfig
const WeChatConfig = require('./WeChatConfig')
var WeChat = require('./wechat')
var wechatApp = new WeChat(WeChatConfig) // 实例wechat 模块
let code = ''

// PC端网页授权
app.get('/userCodePC', function (req, res, next) {
  console.log('--------------------')
  // 0 get code
  let path = 'userInfo'
  // https://open.work.weixin.qq.com/wwopen/sso/qrConnect?appid=ww100000a5f2191&agentid=1000000&redirect_uri=http%3A%2F%2Fwww.oa.com&state=web_login@gyoss9
  let redirectUrl = `http%3A%2F%2F${wechatApp.RedirectUrl}%2F${path}`

  let urlCode = `https://open.work.weixin.qq.com/wwopen/sso/qrConnect?appid=${wechatApp.AppID}&agentid=${wechatApp.AgentID}&redirect_uri=${redirectUrl}&state=STATE`

  console.log('redirectUrl:', redirectUrl)
  res.redirect(urlCode)
})

// 移动端网页授权
app.get('/userCodeMobile', function (req, res, next) {
  console.log('--------------------')
  // 0 get code
  let path = 'userInfo'
  // let redirectUrl = 'https%3A%2F%2Fwww.yunxinyun.cn%2Foauth%2F' + path
  // let redirectUrl = 'https%3A%2F%2Fwww.yunxinyun.cn%2F' + path
  // let redirectUrl = 'https%3A%2F%2Ftevcpx.natappfree.cc%2F' + path
  let redirectUrl = `http%3A%2F%2F${wechatApp.RedirectUrl}%2F${path}`

  let urlCode = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${wechatApp.AppID}&redirect_uri=${redirectUrl}&response_type=code&scope=${wechatApp.Scope}&agentid=${wechatApp.AgentID}&state=STATE#wechat_redirect`

  console.log('redirectUrl:', redirectUrl)
  res.redirect(urlCode)
})

app.get('/userInfo', function (req, res) {
  console.log('--------------------')
  code = req.query.code
  // console.log('req.query', req.query)
  // console.log('code', code)
  // return

  wechatApp.getAccessToken()
    .then(function (accessToken) {
      wechatApp.getUserInfo(accessToken, code)
        .then(function (userId) {
          wechatApp.getUserDetailInfo(accessToken, userId)
            .then(function (userDetail) {
              /*               let content = '<h1>' + userDetail.name + " 的个人信息</h1><p><img src='" + userDetail.avatar + "' /></p>"
                            content = content + `<p><h1>email:${userDetail.email}</h1></p>`
                            content = content + `<p><h1>extra:${userDetail.extattr.attrs[0].name}</h1></p>`
                            content = content + `<p><h1>extra:${userDetail.extattr.attrs[0].value}</h1></p>`
                            res.send(content) */
              res.send(userDetail)
            })
            .catch(function (error) {
              console.log('getUserDetailInfo',error)
              res.send(error)
            })
        })
        .catch(function (error) {
          console.log('getUserInfo:---',error)
          // res.sendStatus(400)
          // res.send(error.errmsg)
          res.send(error)
          // res.status(200).render('error',error);
        })
    })
    .catch(function (error) {
      console.log('getAccessToken',error)
      res.send(error)
    })
})
// -----------------------------------------------------------------------------------------------------------
// handle fallback for HTML5 history API
app.use(require('connect-history-api-fallback')());

// serve webpack bundle output
app.use(devMiddleware);

// enable hot-reload and state-preserving
// compilation error display
app.use(hotMiddleware);

// serve pure static assets
var staticPath = path.posix.join(config.dev.assetsPublicPath, config.dev.assetsSubDirectory);
app.use(staticPath, express.static('./static'));

// var uri = 'http://localhost:' + port
var uri = 'http://g9kn5f.natappfree.cc:' + port

var _resolve
var readyPromise = new Promise(resolve => {
  _resolve = resolve
})

console.log('> Starting dev server...')
devMiddleware.waitUntilValid(() => {
  console.log('> Listening at ' + uri + '\n')
  // when env is testing, don't need open it
  if (autoOpenBrowser && process.env.NODE_ENV !== 'testing') {
    opn(uri)
  }
  _resolve()
})

var server = app.listen(port)

module.exports = {
  ready: readyPromise,
  close: () => {
    server.close()
  }
}
