'use strict' // 设置为严格模式

const
  https = require('https'), // 引入 htts 模块
  request = require('request'),
  fs = require('fs'), // 引入 fs 模块
  accessTokenJson = require('./accessToken') // 引入本地存储的 accessToken

/*
* 构建 WeChat 对象
*/
var WeChat = function (config) {
  this.config = config
  this.AppID = config.AppID
  this.CorpSecret = config.CorpSecret
  this.Scope = config.Scope
  this.AgentID = config.AgentID
  this.RedirectUrl = config.RedirectUrl

  this.requestGet = function (url) {
    return new Promise(function (resolve, reject) {
      https.get(url, function (res) {
        var buffer = [], result = ''
        // 监听 data 事件
        res.on('data', function (data) {
          buffer.push(data)
        })
        // 监听 数据传输完成事件
        res.on('end', function () {
          result = Buffer.concat(buffer).toString('utf-8')
          // 将最后结果返回
          resolve(result)
        })
      }).on('error', function (err) {
        reject(err)
      })
    })
  }

  this.requestPost = function (url, data) {
    return new Promise(function (resolve, reject) {
      https.post(
        { url: url, json: data },
        function (error, response, body) {
          console.log('body:', body)
          if (response.statusCode === 200) {
            resolve(body)
          }
          else {
            reject(error)
          }
        })
    })
  }

  WeChat.prototype.auth = function (req, res) {
    // var that = this;
    // this.getAccessToken().then(function(data){
    //     //格式化请求连接
    //     var url = util.format(that.apiURL.createMenu,that.apiDomain,data);
    //     //使用 Post 请求创建微信菜单
    //     that.getUserDetail(url,JSON.stringify(menus)).then(function(data){
    //         //讲结果打印
    //         console.log(data);
    //     });
    // });

    // 1.获取微信服务器Get请求的参数 signature、timestamp、nonce、echostr
    var signature = req.query.signature, // 微信加密签名
      timestamp = req.query.timestamp, // 时间戳
      nonce = req.query.nonce, // 随机数
      echostr = req.query.echostr// 随机字符串

    // 2.将token、timestamp、nonce三个参数进行字典序排序
    var array = [this.AccessToken, timestamp, nonce]
    array.sort()

    // 3.将三个参数字符串拼接成一个字符串进行sha1加密
    var tempStr = array.join('')
    const hashCode = crypto.createHash('sha1') // 创建加密类型 
    var resultCode = hashCode.update(tempStr, 'utf8').digest('hex') // 对传入的字符串进行加密

    // 4.开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
    if (resultCode === signature) {
      res.send(echostr)
    }
    else {
      res.send('mismatch')
    }
  }

  /*
  * 1. 获取微信 access_token
  */
  WeChat.prototype.getAccessToken = function () {
    var that = this
    return new Promise(function (resolve, reject) {
      // 获取当前时间 
      var currentTime = new Date().getTime()
      // 格式化请求地址
      // var url = util.format(that.apiURL.accessTokenApi, that.apiDomain, that.AppID, that.CorpSecret)
      let urlAccessToken = `https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=${that.AppID}&corpsecret=${that.CorpSecret}`
      // console.log(urlAccessToken)
      // console.log(accessTokenJson)
      // 判断 本地存储的 access_token 是否有效
      // console.log('accessTokenJson.access_token',accessTokenJson.access_token)
      if ( accessTokenJson.access_token === undefined || accessTokenJson.access_token === '' || accessTokenJson.expires_time < currentTime) {
        that.requestGet(urlAccessToken).then(function (data) {
          var result = JSON.parse(data)
          // console.log(result)
          if (result.errcode === 0) {
            accessTokenJson.access_token = result.access_token
            accessTokenJson.expires_time = new Date().getTime() + (parseInt(result.expires_in) - 200) * 1000
            // 更新本地存储的
            fs.writeFile('./accessToken.json', JSON.stringify(accessTokenJson))
            // 将获取后的 access_token 返回
            console.log('将获取后的 access_token 返回:',accessTokenJson)
            resolve(accessTokenJson.access_token)
          }
          else {
            // 将错误返回
            console.log('将错误返回:',result)
            reject(result)
          }
        })
      }
      else {
        // 将本地存储的 access_token 返回
        console.log('本地存储的', accessTokenJson)
        resolve(accessTokenJson.access_token)
      }
    })
  }

  /*
  2. 获取微信 user info
  */
  WeChat.prototype.getUserInfo = function (accessToken, code) {
    var that = this
    console.log('code', code)
    return new Promise(function (resolve, reject) {
      // 2 get user info based on accessToken + code
      let urlUserInfo = `https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=${accessToken}&code=${code}`
      that.requestGet(urlUserInfo)
        .then(function (data) {
          var result = JSON.parse(data)
          console.log('getUserInfo:', result)
          if (result.errcode === 0) {
            // 返回UserId
            if (result.UserId) {
              let UserId = result.UserId
              console.log('UserId', UserId)
              resolve(UserId)
            }
            // // 返回user_ticket
            // if (result.user_ticket) {
            //   let userTicket = result.user_ticket
            //   console.log('userTicket', userTicket)
            //   resolve(userTicket)
            // }
          }
          else {
            reject(result)
          }
        })
    })
  }

  /*
  * 3. 获取微信 user detail info 手机端专用 API
  */
  WeChat.prototype.getUserDetail = function (accessToken, userTicket) {
    let urlUserDetail = `https://qyapi.weixin.qq.com/cgi-bin/user/getuserdetail?access_token=${accessToken}`
    return new Promise(function (resolve, reject) {
      request.post({
        url: urlUserDetail,
        json: { 'user_ticket': userTicket }
      },
      function (error, response, body) {
        console.log('body:', body)
        if (body && body.extattr) { console.log('body:', body.extattr) }
        if (body && body.extattr && body.extattr.attrs) { console.log('body:', body.extattr.attrs[0]) }
        // console.log('body:', body.extattr.attrs[0])

        // console.log('response:', response)
        // res.send('<h1>个人信息</h1>')

        if (response.statusCode === 200) {
          // let userinfo = JSON.parse(body)
          // let userinfo = body
          // 小测试，实际应用中，可以由此创建一个帐户                  
          // response.send('<h1>' + userinfo.name + " 的个人信息</h1><p><img src='" + userinfo.avatar + "' /></p><p>")
          resolve(body)
        }
        else {
          console.log(response.statusCode, error)
          reject(error)
        }
      })
    })
  }

  /*
  * 4. 获取微信 user detail info 网页端专用 API
  */

  // https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid=USERID
  WeChat.prototype.getUserDetailInfo = function (accessToken, userId) {
    var that = this
    let urlUserDetail = `https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=${accessToken}&userid=${userId}`
    return new Promise(function (resolve, reject) {
      that.requestGet(urlUserDetail)
        .then(function (data) {
          var result = JSON.parse(data)
          console.log('getUserDetailInfo:', result)
          if (result.errcode === 0) {
            resolve(result)
          }
          else {
            reject(result)
          }
        })
    })
  }
}
// 暴露可供外部访问的接口
module.exports = WeChat
