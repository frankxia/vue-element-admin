import fetch from '@/utils/fetch'
import Cookies from "js-cookie";
// http://5cwca6.natappfree.cc/userInfo?code=PHTjpciDscAi74cRo38JxwMDzv6H2-MUzrHTdaKbPlg
export function fetchUser() {
  let code = Cookies.get('code')
  return fetch({
    url: `/userInfo?code=${code}`,
    method: 'get'
  })
}

